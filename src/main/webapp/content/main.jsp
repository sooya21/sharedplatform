<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<title>TITLE</title>
<script src="../resources/js/jquery-3.1.1.min.js"></script>
<script src="../resources/js/spin.min.js"></script>
<script src="../resources/js/main.js"></script>
<!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <script>window.html5 || document.write('<script src="js/vendor/html5shiv.js"><\/script>')</script>
<![endif]-->
<link href="../resources/css/common2.css" rel="stylesheet" />
<script src="../resources/js/common.config.js"></script>
<script src="../resources/js/common.js"></script>

</head>
<body>
  <br>
  <div class="account_box">
    <form id="changeForm">
      <input type="text" id="email" placeholder="email" value="email" class="change_input"> <input
        type="password" id="password" value="password" placeholder="비밀번호" class="change_input"> <input
        type="submit" value="로그인" class="pw_submit">
    </form>
  </div>

  <div class="account_box">
    <form id="engamentForm">
      <input type="text" id="code" placeholder="code" value="" class="change_input"> <input
        type="text" id="name" value="" placeholder="name" class="change_input"> <input type="submit"
        value="호출" class="pw_submit">
    </form>
  </div>
  
  <div class="account_box">
    <form id="errorForm">
      <input type="text" id="code" placeholder="code" value="" class="change_input"> <input
        type="text" id="name" value="" placeholder="name" class="change_input"> <input type="submit"
        value="오류발생" class="pw_submit">
    </form>
  </div>
</body>
</html>
<script type="text/javascript">

// object 값을 직접 확인하고 싶은 경우 (param 이나 result 등)
// alert(common.debugObject(params));


// resetUI : 화면(UI) 초기화 관련 로직을 모으고자 하는 의도
// resetUI : 비지니스 로직을 모으고자 하는 의도

	common.setEventHandler({
		initialize : function() {
			$("#changeForm").submit(
					function(event) {
						event.preventDefault();
						var params = common.generateParams("changeForm");
						var successHandler = function(result) {
						};
						common.callService("main/login", 'json', params,
								isAsnc = false, successHandler,
								common.serviceErrorHandler);

					});
			$("#engamentForm").submit(
					function(event) {
						event.preventDefault();
						var params = common.generateParams("engamentForm");
						var successHandler = function(result) {
						};
						common.callService("main/retrieveEngagement", 'json', params,
								isAsnc = false, successHandler, common.serviceErrorHandler);

					});
			
			$("#errorForm").submit(
					function(event) {
						event.preventDefault();
						alert(error);

					});
		},

		resetUI : function() {
		}
	});
</script>
