/**
 *
 * @author Jung Eun-Soo (eun-soo.jung@kr.ey.com)
 */

window.onerror = function myErrorHandler(errorMsg, url, lineNumber) {
	if (!common.DEBUG) {
		return;
	}
	var errorMsg = ("Error occured: " + errorMsg) + "\n";
	errorMsg += ("URL: " + url) + "\n";
	errorMsg += ("Line number: " + lineNumber) + "\n";
	alert(errorMsg);
	common.log(errorMsg, "HIGHLIGHT");
	return false;
};


String.prototype.string = function(len){var s = '', i = 0; while (i++ < len) { s += this; } return s;};
String.prototype.zf = function(len){return "0".string(len - this.length) + this;};
Number.prototype.zf = function(len){return this.toString().zf(len);};
Date.prototype.format = function(f) {
    if (!this.valueOf()) return " ";
 
    var weekName = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
    var d = this;
     
    return f.replace(/(yyyy|yy|MM|dd|E|hh|mm|ss|a\/p)/gi, function($1) {
        switch ($1) {
            case "yyyy": return d.getFullYear();
            case "yy": return (d.getFullYear() % 1000).zf(2);
            case "MM": return (d.getMonth() + 1).zf(2);
            case "dd": return d.getDate().zf(2);
            case "E": return weekName[d.getDay()];
            case "HH": return d.getHours().zf(2);
            case "hh": return ((h = d.getHours() % 12) ? h : 12).zf(2);
            case "mm": return d.getMinutes().zf(2);
            case "ss": return d.getSeconds().zf(2);
            case "a/p": return d.getHours() < 12 ? "오전" : "오후";
            default: return $1;
        }
    });
};


var common = common || {};

common.DEBUG = false;
if (!(typeof CFG_DEBUG === "undefined")) {
	common.DEBUG = CFG_DEBUG; 
}

common.RUN_MODE = "DESIGN"; // DESIGN/CLIENT/SERVER
if (!(typeof CFG_RUN_MODE === "undefined")) {
	common.RUN_MODE = CFG_RUN_MODE; 
}
common.preURL = "";
if (!(typeof CFG_preURL === "undefined")) {
	common.preURL = CFG_preURL; 
}

common.loginPage = "my_main.html";
common.appendedLogInfo = "";
common.eventHandlers = null;
common.resultProviders = null;

common.spinner = null;
//common.spinnerTarget = null;
common.opts = {
	lines : 11, // The number of lines to draw
	length : 8, // The length of each line
	width : 4, // The line thickness
	radius : 8, // The radius of the inner circle
	corners : 1, // Corner roundness (0..1)
	rotate : 0, // The rotation offset
	direction : 1, // 1: clockwise, -1: counterclockwise
	color : '#000', // #rgb or #rrggbb or array of colors
	speed : 1, // Rounds per second
	trail : 60, // Afterglow percentage
	shadow : true, // Whether to render a shadow
	hwaccel : false, // Whether to use hardware acceleration
	className : 'spinner', // The CSS class to assign to the spinner
	zIndex : 2e9, // The z-index (defaults to 2000000000)
	top : 'auto', // Top position relative to parent in px
	left : 'auto' // Left position relative to parent in px
};

common.startProgressIndicator = function() {
	$('body')
			.append(
					'<div id="spin_modal_overlay" style="background: rgba(255,255,255, .5); background-color: #fff; opacity: 0.5; filter: alpha(opacity=50); -ms-filter:progid:DXImageTransform.Microsoft.Alpha(Opacity=50); width:100%; height:100%; position:fixed; top:0px; left:0px; z-index:'
							+ (2e9 - 1) + '"/>');
	var spinElem = $("#spin_modal_overlay")[0];
	if (common.spinner == null) {
		common.spinner = new Spinner(common.opts).spin(spinElem);
	}
	common.spinner.spin(spinElem);
};

common.stopProgressIndicator = function() {
	common.spinner.stop();
	$("#spin_modal_overlay").remove();
};

common.getImageURL = function(fileName) {
	var subDir = fileName.substring(0, 1);
	return "../img/procedure/" + subDir + "/"+ fileName;
};

common.getThumbImageURL = function(fileName) {
	var subDir = fileName.substring(0, 1);
	return "../img/thumbnail/" + subDir + "/"+ fileName;
};

common.getFileExtension = function(filename) {
	return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename)[0] : undefined;
};

common.initializeDebug = function(){
	if (!common.DEBUG) {
		return;
	}
	
	var html = "";
	html += "<div id='debugZoneBg' class='debugAreaBg'>";
	html += "</div>";
	html += "<div id='debugZone' class='debugArea'>";
	html += "<table width='100%' cellspacing='0' cellpadding='0' border='1'>";
	html += "<tbody>";
	html += "<tr>";
	html += "<td width='88%'>";
	html += "<div id='logArea' style='height: 286px; width: 100%; overflow-x: scroll; overflow-y: scroll; white-space:pre-wrap; font-size: 0.8em;line-height: 1.4em; font-family: 맑은 고딕;'></div>";
	html += "</td>";
	html += "<td bgcolor='white' width='200' style='text-align: left; vertical-align: top'>";
	html += "<br>" + common.RUN_MODE + " Mode<br><br>";
	html += "<input type='button' style='height:30px;width:100%' value='Top' onclick='javascript:$(\"#logArea\").scrollTop(0)'><br>";
	html += "<input type='button' style='height:30px;width:100%' value='Bottom' onclick='javascript:$(\"#logArea\").scrollTop($(\"#logArea\").prop(\"scrollHeight\"));'><br>";
	html += "<input type='button' style='height:30px;width:100%' value='Set BackColor' onclick='javascript:$(\"#logArea\").css(\"background-color\", \"white\");'><br>";
	html += "<input type='button' style='height:30px;width:100%' value='Clear' onclick='javascript:$(\"#logArea\").html(\"\");$(\"#logArea\").css(\"background-color\", \"\")'><br><br>";
	html += "<input type='button' style='height:30px;width:100%' value='Refresh' onclick='common.reloadNoCache()'><br><br>";
	html += "<input type='button' style='height:30px;width:100%' value='Enable Drag' onclick=''><br>";
	html += "<input type='button' style='height:30px;width:100%' value='Disable Drag' onclick=''><br>";
//	html += "<input type='button' style='height:30px;width:100%' value='Enable Drag' onclick='$(\"#debugZone\").draggable({ disabled: false });'><br>";
//	html += "<input type='button' style='height:30px;width:100%' value='Disable Drag' onclick='$(\"#debugZone\").draggable({ disabled: true });'><br>";
	html += "</td>";
	html += "</tr>";
	html += "</tbody>";
	html += "</table>";
	html += "</div>";
	$('body').prepend(html);
//	$("#debugZone").draggable();
	setInterval(common.loggingJob, 400);
};

common.log = function(info, type) {
	if (!common.DEBUG) {
		return;
	}
	if (typeof (info) == 'object') {
		info = common.debugObject(info);
	}
	info += "###"; 
	info += type;
	common.appendedLogInfo += (info + "|||");
};

common.loggingJob = function() {
	if (common.appendedLogInfo) {
		var logData = common.appendedLogInfo;
		common.appendedLogInfo = "";
		logDataArr = logData.split("|||");
		for ( var i = 0; i < logDataArr.length; i++) {
			common.writeLog(logDataArr[i]);
		}
	}
};

common.writeLog = function(info) {
	if (!common.DEBUG) {
		return;
	}
	var infoArr = info.split("###");
	var logContent = infoArr[0];
	var type = infoArr[1]; 
	var convertedInfo = "";
	if (type == "HIGHLIGHT") {
		convertedInfo = "<span style='background-color: #FFFF00'>" + logContent + "</span><br><br>";
	} else {
		convertedInfo = "<span style='background-color: #FFFFFF'>" + logContent + "</span><br><br>";
	}
//	var convertedInfo = "<mark>" + info + "<mark><br>";
	$("#logArea").append(convertedInfo);
	var elem = document.getElementById('logArea');
//	elem.scrollTop = elem.scrollHeight;
	$("#logArea").animate({
	    scrollTop: elem.scrollHeight
	}, 100);

};

common.serviceErrorHandler = function(jqXHR, textStatus, errorThrown) {
	
	if (jqXHR.responseJSON != null) {
		var responseJson = jqXHR.responseJSON;
		var code = responseJson["code"];
		var message = responseJson["message"];
		if (code == "B0002") {
			window.location = common.loginPage;
		} else {
//			var displayMessage = "오류 코드: " + code + "\n\n";
//			displayMessage += "오류 내용: " + message + "\n\n\n\n";
			var displayMessage = message + "\n\n";
			alert(displayMessage);
		}
	} else if (jqXHR.responseText != null) {
		var originalErrMsg = jqXHR.responseText;
		var tmpErrorMessage = originalErrMsg.replace("\"","").replace("\"","");
		var errorCommonMsg = "System Error. \nContact to System Administrator";
		errorDisplayMessage = errorCommonMsg + 
        "\n\nErrorMessage : "+tmpErrorMessage;
		console.log(tmpErrorMessage);
		if (common.DEBUG) {
			alert(errorDisplayMessage);
		} else {
			alert("서버 오류발생\n관리자에게 문의 바랍니다.");
		}
	} else {
		alert(common.debugObject(jqXHR));
	}
 	
//	var originalErrMsg = jqXHR.responseText;
//	
//	var tmpErrorMessage = originalErrMsg.replace("\"","").replace("\"","");
//	var errorArray= tmpErrorMessage.split("||");
//	var errorCommonMsg = "System Error. \nContact to System Administrator";
//	var errorDisplayMessage = "";
//	if(errorArray.length = 2){
//		var errorMessage = '';
//		if(errorArray[0] != null && errorArray[0] != '') {
//			errorMessage = errorArray[0].replace(/\\n/g,"\n");
//		}
//		errorCode = errorArray[1]; 
//		errorDisplayMessage = errorCommonMsg + 
//        						"\n\nErrorMessage : "+ ""+errorMessage+"" + "\n\nErrorCode : "+errorCode;
//	}else{
//		errorDisplayMessage = errorCommonMsg + 
//	                      "\n\nErrorMessage : "+tmpErrorMessage;
//	}
//	common.alert(errorDisplayMessage, "Service error", common.handleServiceError(errorCode));
//	alert(errorDisplayMessage);
};
common.debugObject = function(arr, level) {
	var dumped_text = "";
	if (!level)
		level = 0;

	// The padding given at the beginning of the line.
	var level_padding = "";
	for ( var j = 0; j < level + 1; j++)
		level_padding += "    ";

	if (typeof (arr) == 'object') { // Array/Hashes/Objects
		for ( var item in arr) {
			var value = arr[item];
			if (typeof (value) == 'object') { // If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += common.debugObject(value, level + 1);
			} else {
				dumped_text += level_padding + "'" + item + "' : \"" + value + "\",\n";
			}
		}
	} else { // Stings/Chars/Numbers etc.
		dumped_text = "===>" + arr + "<===(" + typeof (arr) + ")";
	}
	return dumped_text;
};

common.setEventHandler = function(handlers) {
	common.eventHandlers = handlers;
};

common.addEventHandler = function(handlers) {
	for ( var event in handlers) {
		var func = handlers[event];
		common.eventHandlers[event] = func;
	}
};

common.getEventHandlers = function() {
	return common.eventHandlers;
};

common.setResultProvider = function(providers) {
	common.resultProviders = providers;
};

common.callService = function(url, dataType, params, isAsnc, successHandler, errorHandler) {
	var fullUrl = common.preURL + url;
	var serviceInfo = fullUrl + ", dataType:" + dataType + ", isAsnc:" + isAsnc + "\n";
	serviceInfo += ("Request params for " + fullUrl + ":\n" + common.debugObject(params));
	if (common.log) {
		common.log("common.callService was called as " + common.RUN_MODE + " mode");
		common.log(serviceInfo);
	}
	var successHandler2 = function(result) {
		var returnInfo = "Response result for " + fullUrl + ":\n" + common.debugObject(result);
		common.log(returnInfo);
		successHandler(result);
	}
	var beforeSend = null;
	var complete = null;
	if (!isAsnc) {
		beforeSend = common.startProgressIndicator;
		complete = common.stopProgressIndicator;
		isAsnc = true;
	}
	if (common.RUN_MODE == "SERVER") {
		$.ajax({
			url : fullUrl,
			data : JSON.stringify(params),
			dataType : dataType,
			type : 'POST',
			contentType : 'application/' + dataType,
			context : this,
			async : isAsnc,
			beforeSend : beforeSend,
			complete : complete,
			success : successHandler2,
			error : errorHandler
		});
	} else if (common.RUN_MODE == "CLIENT") {
		if (common.resultProviders[url] == undefined) {
			common.log("ResultProvider Not Found: \"" + url + "\"", "HIGHLIGHT");
			printResultProviderCode(url);
		}
		var result = common.resultProviders[url]();
		common.log("ResultProvider \"" + url + "\" was called as " + common.RUN_MODE + " mode");
		successHandler2(result);
	}
};

common.callUpload = function(url, formData, successHandler, errorHandler) {
	var fullUrl = common.preURL + url;
	var serviceInfo = fullUrl;
//	serviceInfo += ("Request params: " + common.debugObject(params));
	if (common.log) {
		common.log(serviceInfo);
	}
	var successHandler2 = function(result) {
		var returnInfo = "Response params: " + common.debugObject(result);
		common.log(returnInfo);
		successHandler(result);
	}
	var beforeSend = null;
	var complete = null;
	var isAsnc = false;
	if (!isAsnc) {
		beforeSend = common.startProgressIndicator;
		complete = common.stopProgressIndicator;
		isAsnc = true;
	}
	$.ajax({
		url : fullUrl,
		processData : false,
		contentType : false,
		data : formData,
		type : 'POST',
		async : isAsnc,
		beforeSend : beforeSend,
		complete : complete,
		success : successHandler2,
		error : errorHandler
	});
};

common.generateParams = function(formId) {
	var params = {};
	var target = "";
	if (formId) {
		target = "#" + formId;
	}
	if ($(target).length == 0) {
		throw new Error("Selector is not existed: " + formId);
	}
	$(
			target + " input:text, " + target + " input:password, " + target + " input:hidden, " + target + " input:checkbox, " + target
					+ " input:radio, " + target + " select, " + target + " textarea").each(function() {
		var ignore = $(this).attr('ignore');
		if (ignore == "true") {
			return;
		}
		var tagName = $(this).get(0).tagName;
		var type = $(this).attr('type');
		var id = "";
		if (tagName == "INPUT" && (type == "radio" || type == "checkbox")) {
			id = $(this).attr("name");
		} else {
			id = $(this).attr("id");
		}
		if (id == undefined) {
			return;
		}
		if (tagName == "INPUT" || tagName == "TEXTAREA") {
			var type = $(this).attr('type');
			var subtype = $(this).attr('subtype');
			if (type == "radio") {
				params[id] = $(":input:radio[name=" + id + "]:checked").val();
			} else if (type == "checkbox") {
				$("input:checkbox[id=" + id + "]").is(":checked") ? (params[id] = "Y") : (params[id] = "N");
			} else if (subtype == "datetimepicker" || subtype == "datepicker" || subtype == "monthpicker") {
				var date = $("#" + id).val();
				params[id] = common.modifyDateFormat(date);
			} else {
				params[id] = $(this).val().replace(/\n/g, '<br>');
			}
		} else if (tagName == "SELECT") {
			params[id] = $(this).val();
		}
	});
//	common.log("function called - common.generateParams(" + common.debugObject(params) + ")");
	return params;
};

common.getQueryParam = function(param) {
	var result = window.location.search.match(new RegExp("(\\?|&)" + param
			+ "(\\[\\])?=([^&]*)"));
	return result ? result[3] : false;
};

common.AutoRetriever = function(endpoint, formId, successHandler, resetHandler, limit) {
	var page = 1;
	var inProgress = false;
	var successHandler = successHandler;
	var retrieveCondition = function() {
		return true;
	}
	$(window).scroll(
			function() {
				if ($(window).scrollTop() + window.innerHeight == $(document)
						.height()) {
					if (!retrieveCondition()) {
						return;
					}
					if (inProgress) {
						return;
					}
					retrieveInternal();
				}
			});
	 var wrapSuccessHandler = function(result) {
		successHandler(result);
		inProgress = false;
		if (jQuery.isEmptyObject(result)) {
			common.log("Result is empty.")
	       return;
	    }
		common.log("Page " + page + " retrieved.")
		page++;
	}
	 
	var retrieveInternal = function() {
		inProgress = true;
		var params = {};
		if (formId != "") {
			params = common.generateParams(formId);
		}
		params["page"] = page;
		params["limit"] = limit;
		common.callService(endpoint, 'json', params,
				isAsnc = true, wrapSuccessHandler,
				common.serviceErrorHandler);
	}
	this.retrieve = function(reset) {
		if (reset) {
			page = 1;
			resetHandler();
		}
		retrieveInternal();
	}
	
	this.setRetrieveCondition = function(handler) {
		retrieveCondition = handler;
	}
	
}

document.onreadystatechange = function() {
	if (document.readyState === 'complete') {
		common.initializeDebug();
//		if (common.RUN_MODE == "DESIGN") {
//			$('.invisible').each(function() {
//				$(this).show();
//			});
//		}
		if (common.RUN_MODE == "CLIENT" || common.RUN_MODE == "SERVER") {
			if (common.getEventHandlers() == null) {
				common.log("Function Not Found: \"common.setEventHandler\"", "HIGHLIGHT");
				printEventHandlersCode();
				return;
			}

			if (common.getEventHandlers().resetUI == undefined) {
				common.log("Function Not Found: \"resetUI\"", "HIGHLIGHT");
				printEventHandlersCode();
			} else {
				common.getEventHandlers().resetUI();
				common.log("Function \"resetUI\" called.");
			}
			if (common.getEventHandlers().initialize == undefined) {
				common.log("Function Not Found: \"initialize\"", "HIGHLIGHT");
				printEventHandlersCode();
			} else {
				common.getEventHandlers().initialize();
				common.log("Function \"initialize\" called.");
			}
		}
		$('.invisible').each(function() {
			$(this).show();
		});
		common.log("Invisible components were visibled.");

	}
}

common.dialogCnt = 0;
common.alert = function(content, title, okHandler) {
	if (!content) {
		content = "";
	}
	var dialogId = "dialog-confirm" + common.dialogCnt++;
	var html = "";
	html += "<div id='" + dialogId + "' title=''>";
	html += "<p>";
	html += "<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span><div id='text_div'></div>";
	html += "</p>";
	html += "</div>";
	$("body").append(html);
	if (!title) {
		title = "알림";
	}
	$("#" + dialogId).attr("title", title);
	//	$("#dialog-confirm #icon").attr("class", "ui-icon ui-icon-alert");
	var textDiv = $("#" + dialogId + " > #text_div");
	content = content.replace(new RegExp('\r?\n', 'g'), '<br />');
	textDiv.html(content);
	$("#" + dialogId).dialog({
		resizable : false,
		height : 208,
		modal : true,
		//		autoOpen: false,
		buttons : {
			닫기 : function() {
				$(this).dialog("close");
				if (okHandler) {
					okHandler();
				}
			},
		},
		open : function(event, ui) {
			var dialogs = $('.ui-dialog:visible');
			if (dialogs.length > 1) {
				dialogs.each(function(i, e) {
					if (i === 0) {
						//	                   $(e).css('top', '0px');
					} else {
						var prevDialog = $(e).prevAll('.ui-dialog:visible');
						$(e).css('top', parseInt(prevDialog.css('top')) + 20 + 'px');
						$(e).css('left', parseInt(prevDialog.css('left')) + 20 + 'px');
					}
				});
			}
		}

	});
	$("#" + dialogId).dialog("open");
};

common.loadFiles = function(files, callback) {
	var progress = 0;
	files.forEach(function(file) {
		var extension = common.getFileExtension(file);
		var fileref = undefined;
		if (extension == "js") {
			fileref = document.createElement("script")
	        fileref.setAttribute("type","text/javascript")
	        fileref.setAttribute("src", file);
		} else if (extension == "css") {
			fileref = document.createElement("link")
	        fileref.setAttribute("rel", "stylesheet")
	        fileref.setAttribute("type", "text/css")
	        fileref.setAttribute("href", file)
		}
		fileref.onload = function() {
			if (++progress == files.length) {
				callback();
			}
		};
		if (typeof fileref!="undefined") {
	        document.getElementsByTagName("head")[0].appendChild(fileref)
		}
	});
}



var js = [ "../css/jquery-ui.min.css","../js/jquery-ui.min.js", "../js/spin.min.js" ];
common.loadFiles(js, function() {
	common.log("Files were loaded");
});

common.reloadNoCache = function() {
	$.ajax({
		url : window.location.href,
		headers : {
			"Pragma" : "no-cache",
			"Expires" : -1,
			"Cache-Control" : "no-cache"
		}
	}).done(function() {
		window.location.reload(true);
	});
}

function printEventHandlersCode() {
	var logMsg = "";
	logMsg += "------------ CODE Start --------------------------\n";
	logMsg += "	common.setEventHandler({\n";
	logMsg += "		initialize : function() {\n";
	logMsg += "		},\n";
	logMsg += "		resetUI : function() {\n";
	logMsg += "		}\n";
	logMsg += "	});\n";
	logMsg += "------------ CODE End ---------------------------\n";	
	common.log(logMsg, "HIGHLIGHT");
}

function printResultProviderCode(provider) {
	var logMsg = "";
	logMsg += "------------ CODE Start --------------------------\n";
	logMsg += "	common.setResultProvider({\n";
	logMsg += "		getDesignsByParams : function() {\n";
	logMsg += "			var result = new Array();\n";
	logMsg += "			result[0] = {\n";
	logMsg += "				'a' : \"A\",\n";
	logMsg += "			}\n";
	logMsg += "			return result;\n";
	logMsg += "		},\n";
	logMsg += "	});\n";
	logMsg += "------------ CODE End ---------------------------\n";	
	common.log(logMsg, "HIGHLIGHT");
}
