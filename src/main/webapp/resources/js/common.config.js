/**

 * @author Jung Eun-Soo (eun-soo.jung@kr.ey.com)
 */


/**
 * 개발 환경용 설정
 */
var CFG_DEBUG = true;			// true or false
var CFG_RUN_MODE = "SERVER";	// DESIGN or CLIENT or SERVER
var CFG_preURL = "/sharedplatform/";


/**
 * 운영 환경용 설정
 */
//var CFG_DEBUG = false;			// true or false
//var CFG_RUN_MODE = "SERVER";	// DESIGN or CLIENT or SERVER
//var CFG_preURL = "/rest/";
