package com.ey.kr.digitalhub.sharedplatform.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ey.kr.digitalhub.sharedplatform.service.ExampleService;
import com.ey.kr.digitalhub.sharedplatform.vo.BusinessTripEngagementVo;

@Controller
@RequestMapping("/main")
public class ExampleController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private ExampleService exampleService;
    
	@RequestMapping(value = "/home", method = RequestMethod.GET)
//	public String home(Authentication authentication, HttpSession session, Locale locale, Model model) {
	public String home(HttpSession session, Locale locale, Model model) {
	    logger.info("Welcome home! The client locale is {}.", "locale"); 
	    
	    BusinessTripEngagementVo engagementVo = new BusinessTripEngagementVo();
	    List<BusinessTripEngagementVo> engamentList = exampleService.retrieveEngagement(engagementVo);
		return "index";
	}
}
