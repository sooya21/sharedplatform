/**
 * @author EY eun-soo.jung
 * @since 2020. 2. 12.
 * @version 1.0
 *
 *
 * Data				Author				Note
 * ---------------  ------------------  -------------------------------------
 *  2020. 2. 12.	eun-soo.jung		최초 생성
 *
 *
 * Copyright © 2020 Ernst & Young Global Limited. All right reserved.
*/

package com.ey.kr.digitalhub.sharedplatform.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.kr.digitalhub.sharedplatform.mapper.ExampleMapper;
import com.ey.kr.digitalhub.sharedplatform.vo.BusinessTripEngagementVo;
import com.ey.kr.digitalhub.sharedplatform.vo.ExampleVo;

@Service
public class ExampleServiceImpl implements ExampleService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ExampleMapper exampleMapper;

    @Override
    public int createExample(ExampleVo exampleVo) {
        return exampleMapper.insertExample(exampleVo);
    }

    @Override
    public ExampleVo retrieveExampleByUid(String uid) {
        return exampleMapper.selectExampleByUid(uid);
    }

    @Override
    public int updateExample(ExampleVo exampleVo) {
        return exampleMapper.updateExample(exampleVo);
    }

    @Override
    public int deleteExampleByUid(String uid) {
        return exampleMapper.deleteExampleByUid(uid);
    }

    @Override
    public int deleteExample(ExampleVo exampleVo) {
        return exampleMapper.deleteExample(exampleVo);
    }

    @Override
    public int createExampleList(List<ExampleVo> exampleVoList) {
        return exampleMapper.insertExampleList(exampleVoList);
    }

    @Override
    public List<ExampleVo> retrieveExampleList(ExampleVo exampleVo) {
        return exampleMapper.selectExampleList(exampleVo);
    }
    
    
    //DB 접속 확인 목적으로 EY Korea portal 에서 카피/페이스트
    public List<BusinessTripEngagementVo> retrieveEngagement(BusinessTripEngagementVo engagementVo) {
        return exampleMapper.selectEngagement(engagementVo);
    }

}
