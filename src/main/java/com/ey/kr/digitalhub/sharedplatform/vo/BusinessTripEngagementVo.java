/**
 * @author EY eun-soo.jung
 * @since 2020. 2. 14.
 * @version 1.0
 *
 *
 * Data				Author				Note
 * ---------------  ------------------  -------------------------------------
 *  2020. 2. 14.	eun-soo.jung		최초 생성
 *
 *
 * Copyright © 2020 Ernst & Young Global Limited. All right reserved.
*/

package com.ey.kr.digitalhub.sharedplatform.vo;

public class BusinessTripEngagementVo {

private String uid;
    
    private String code;
    private String name;
    private String clientCode;
    private String clientName;
    private String bu;
    private String mu;
    private String ou;
    private String smu;
    private String eTypeCls;
    private String globalServiceCode;
    private String globalServiceCodeName;
    private String engagementTypeDescription;
    private String departmentName;
    
    public String getUid() {
        return uid;
    }
    public void setUid(String uid) {
        this.uid = uid;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getClientCode() {
        return clientCode;
    }
    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }
    public String getClientName() {
        return clientName;
    }
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
    public String getBu() {
        return bu;
    }
    public void setBu(String bu) {
        this.bu = bu;
    }
    public String getMu() {
        return mu;
    }
    public void setMu(String mu) {
        this.mu = mu;
    }
    public String getOu() {
        return ou;
    }
    public void setOu(String ou) {
        this.ou = ou;
    }
    public String getSmu() {
        return smu;
    }
    public void setSmu(String smu) {
        this.smu = smu;
    }
    public String geteTypeCls() {
        return eTypeCls;
    }
    public void seteTypeCls(String eTypeCls) {
        this.eTypeCls = eTypeCls;
    }
    public String getGlobalServiceCode() {
        return globalServiceCode;
    }
    public void setGlobalServiceCode(String globalServiceCode) {
        this.globalServiceCode = globalServiceCode;
    }
    public String getGlobalServiceCodeName() {
        return globalServiceCodeName;
    }
    public void setGlobalServiceCodeName(String globalServiceCodeName) {
        this.globalServiceCodeName = globalServiceCodeName;
    }
    public String getEngagementTypeDescription() {
        return engagementTypeDescription;
    }
    public void setEngagementTypeDescription(String engagementTypeDescription) {
        this.engagementTypeDescription = engagementTypeDescription;
    }
    public String getDepartmentName() {
        return departmentName;
    }
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    
    
}
