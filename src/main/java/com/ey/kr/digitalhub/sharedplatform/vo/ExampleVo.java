/**
 * @author EY eun-soo.jung
 * @since 2020. 2. 13.
 * @version 1.0
 *
 *
 * Data				Author				Note
 * ---------------  ------------------  -------------------------------------
 *  2020. 2. 13.	eun-soo.jung		최초 생성
 *
 *
 * Copyright © 2020 Ernst & Young Global Limited. All right reserved.
*/

package com.ey.kr.digitalhub.sharedplatform.vo;

public class ExampleVo {

    private String uid;
    private String name;
    private String description;
    /**
     * @return the uid
     */
    public String getUid() {
        return uid;
    }
    /**
     * @param uid the uid to set
     */
    public void setUid(String uid) {
        this.uid = uid;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
