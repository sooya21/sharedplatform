/**
 * @author EY eun-soo.jung
 * @since 2020. 2. 14.
 * @version 1.0
 *
 *
 * Data				Author				Note
 * ---------------  ------------------  -------------------------------------
 *  2020. 2. 14.	eun-soo.jung		최초 생성
 *
 *
 * Copyright © 2020 Ernst & Young Global Limited. All right reserved.
*/

package com.ey.kr.digitalhub.sharedplatform.vo;

public class UserVo {

    private String email;
    private String password;

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
}
