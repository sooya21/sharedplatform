/**
 * @author EY eun-soo.jung
 * @since 2020. 2. 13.
 * @version 1.0
 *
 *
 * Data				Author				Note
 * ---------------  ------------------  -------------------------------------
 *  2020. 2. 13.	eun-soo.jung		최초 생성
 *
 *
 * Copyright © 2020 Ernst & Young Global Limited. All right reserved.
*/

package com.ey.kr.digitalhub.sharedplatform.dao;

import java.util.List;

import com.ey.kr.digitalhub.sharedplatform.common.BaseSqlDao;
import com.ey.kr.digitalhub.sharedplatform.vo.BusinessTripEngagementVo;
import com.ey.kr.digitalhub.sharedplatform.vo.ExampleVo;

public class ExampleDaoImpl extends BaseSqlDao implements ExampleDao {
    
    public int insertExample(ExampleVo exampleVo) {
        return sqlSession.insert("exampleSql.insertExample", exampleVo);
    }
    
    public ExampleVo selectExampleByUid(String uid) {
        return sqlSession.selectOne("exampleSql.selectExampleByUid", uid);
    }
    
    public int updateExample(ExampleVo exampleVo) {
        return sqlSession.update("exampleSql.updateExample", exampleVo);
    }
    
    public int deleteExampleByUid(String uid) {
        return sqlSession.delete("exampleSql.deleteExampleByUid", uid);
    }
    
    public int deleteExample(ExampleVo exampleVo) {
        return sqlSession.delete("exampleSql.deleteExample", exampleVo);
    }
    
    public int insertExampleList(List<ExampleVo> exampleVoList) {
        return sqlSession.insert("exampleSql.insertExampleList", exampleVoList);
    }
    
    public List<ExampleVo> selectExampleList(ExampleVo exampleVo) {
        return sqlSession.selectList("exampleSql.selectExampleList", exampleVo);
    }
    
    public List<BusinessTripEngagementVo> selectEngagement(BusinessTripEngagementVo engagementVo) {
        return sqlSession.selectList("businessTripSql.selectEngagement", engagementVo);
    }
    

}
