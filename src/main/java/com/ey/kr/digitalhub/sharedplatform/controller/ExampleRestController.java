package com.ey.kr.digitalhub.sharedplatform.controller;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ey.kr.digitalhub.sharedplatform.service.ExampleService;
import com.ey.kr.digitalhub.sharedplatform.vo.BusinessTripEngagementVo;
import com.ey.kr.digitalhub.sharedplatform.vo.UserVo;

@RestController
@RequestMapping("/main")
public class ExampleRestController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ExampleService exampleService;

    @RequestMapping("/login")
    public UserVo login(@RequestBody Map map, HttpServletRequest request, HttpServletResponse response) {
        String email = (String) map.get("email");
        String password = (String) map.get("password");
//        UserVo userVo = userService.login(email, password); 
        UserVo userVo = new UserVo();
        userVo.setEmail(email);
        userVo.setPassword(password);
        HttpSession session = request.getSession();
        session.setAttribute("SESSION_USER", userVo);
        return userVo;
    }

    @RequestMapping("/retrieveEngagement")
    public List<BusinessTripEngagementVo> retrieveEngagement(@RequestBody BusinessTripEngagementVo engagementVo,
            HttpServletRequest request, HttpServletResponse response) {
//        BusinessTripEngagementVo engagementVo = new BusinessTripEngagementVo();
        List<BusinessTripEngagementVo> engamentList = exampleService.retrieveEngagement(engagementVo);
        return engamentList;
    }
}
