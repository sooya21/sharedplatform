/**
 * @author EY eun-soo.jung
 * @since 2020. 2. 14.
 * @version 1.0
 *
 *
 * Data				Author				Note
 * ---------------  ------------------  -------------------------------------
 *  2020. 2. 14.	eun-soo.jung		최초 생성
 *
 *
 * Copyright © 2020 Ernst & Young Global Limited. All right reserved.
*/

package com.ey.kr.digitalhub.sharedplatform.mapper;

import java.util.List;

import com.ey.kr.digitalhub.sharedplatform.vo.BusinessTripEngagementVo;
import com.ey.kr.digitalhub.sharedplatform.vo.ExampleVo;

public interface ExampleMapper {

    public int insertExample(ExampleVo exampleVo);
    
    public ExampleVo selectExampleByUid(String uid);
    
    public int updateExample(ExampleVo exampleVo);
    
    public int deleteExampleByUid(String uid);
    
    public int deleteExample(ExampleVo exampleVo);
    
    public int insertExampleList(List<ExampleVo> exampleVoList);
    
    public List<ExampleVo> selectExampleList(ExampleVo exampleVo);
    
    public List<BusinessTripEngagementVo> selectEngagement(BusinessTripEngagementVo engagementVo);
    
}
