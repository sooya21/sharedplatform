/**
 * @author EY eun-soo.jung
 * @since 2020. 2. 12.
 * @version 1.0
 *
 *
 * Data				Author				Note
 * ---------------  ------------------  -------------------------------------
 *  2020. 2. 12.	eun-soo.jung		최초 생성
 *
 *
 * Copyright © 2020 Ernst & Young Global Limited. All right reserved.
*/

package com.ey.kr.digitalhub.sharedplatform.service;

import java.util.List;

import com.ey.kr.digitalhub.sharedplatform.vo.BusinessTripEngagementVo;
import com.ey.kr.digitalhub.sharedplatform.vo.ExampleVo;

public interface ExampleService {
    
    public int createExample(ExampleVo exampleVo);
    
    public ExampleVo retrieveExampleByUid(String uid);
    
    public int updateExample(ExampleVo exampleVo);
    
    public int deleteExampleByUid(String uid);
    
    public int deleteExample(ExampleVo exampleVo);
    
    public int createExampleList(List<ExampleVo> exampleVoList);
    
    public List<ExampleVo> retrieveExampleList(ExampleVo exampleVo);
    
    public List<BusinessTripEngagementVo> retrieveEngagement(BusinessTripEngagementVo engagementVo);
}
