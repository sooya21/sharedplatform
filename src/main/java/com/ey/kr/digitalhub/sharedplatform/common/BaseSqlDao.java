/**
 * @author EY eun-soo.jung
 * @since 2020. 2. 13.
 * @version 1.0
 *
 *
 * Data				Author				Note
 * ---------------  ------------------  -------------------------------------
 *  2020. 2. 13.	eun-soo.jung		최초 생성
 *
 *
 * Copyright © 2020 Ernst & Young Global Limited. All right reserved.
*/

package com.ey.kr.digitalhub.sharedplatform.common;

import org.apache.ibatis.session.SqlSession;

public class BaseSqlDao {
    
    protected SqlSession sqlSession;

    /**
     * @return the sqlSession
     */
    public SqlSession getSqlSession() {
        return sqlSession;
    }

    /**
     * @param sqlSession the sqlSession to set
     */
    public void setSqlSession(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }
    
    
}
